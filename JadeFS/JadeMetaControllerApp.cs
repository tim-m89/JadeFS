/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

namespace JadeMetaExposure
{
	using System;
	using System.Collections.Generic;
	using System.Runtime.Serialization;
	using JadeSoftware.Joob;
	using JadeSoftware.Joob.Client;
	using JadeSoftware.Joob.Metadata;
	using JadeSoftware.Joob.Management;
	using JadeSoftware.Joob.MetaSchema;
	using JadeSoftware.Jade.DotNetInterop;

	[System.Runtime.Serialization.DataContractAttribute(IsReference=true)]
	[System.ComponentModel.DataAnnotations.MetadataTypeAttribute(typeof(JadeMetaControllerAppMetadata))]
	[JadeSoftware.Joob.Client.JoobClassAttribute("JadeMetaControllerApp", "RootSchema.JadeMetaControllerSchema", ClassNamespace="JadeMetaExposure")]
	[JadeSoftware.Joob.Metadata.JomlTypeAttribute(JadeSoftware.Joob.Metadata.JomlTypeKind.Class, "JadeMetaControllerApp", typeof(JoobObject))]
	public partial class JadeMetaControllerApp : JoobObject
	{
		private static JadeMetaControllerAppMetadata _metaModel;
		partial void _initialize();
		static JadeMetaControllerApp()
		{
			_metaModel = MetadataCache<JadeMetaControllerAppMetadata>.GetData(null);
		}
		public JadeMetaControllerApp() :
			this(JadeSoftware.Joob.ClassPersistence.Persistent)
		{
		}
		public JadeMetaControllerApp(JadeSoftware.Joob.ClassPersistence lifetime) :
			base(lifetime, typeof(JadeMetaControllerApp), _metaModel.metaClass)
		{
			this._initialize();
		}
		protected JadeMetaControllerApp(JadeSoftware.Joob.ClassPersistence lifetime, System.Type type, JadeSoftware.Joob.ClassMetadata metaClass) :
			base(lifetime, type, metaClass)
		{
			this._initialize();
		}

        #region Jade Methods

        [JadeSoftware.Joob.Client.JoobMethodAttribute("schemaGetClasses")]
        public ClassNDict SchemaGetClasses(Schema schema)
        {
            using (JadeParam retnParam = new JadeParamObject(Usage.Output),
                            jadeParam1 = new JadeParamObject(schema))
            {
                this.SendMessage(_metaModel.schemaGetClasses, retnParam, jadeParam1);
                return (retnParam as JadeParamObject).Value as ClassNDict;
            }

        }

        [JadeSoftware.Joob.Client.JoobMethodAttribute("typeGetMethods")]
        public MethodNDict TypeGetMethods(JomType type)
        {
            using (JadeParam retnParam = new JadeParamObject(Usage.Output),
                            jadeParam1 = new JadeParamObject(type))
            {
                this.SendMessage(_metaModel.typeGetMethods, retnParam, jadeParam1);
                return (retnParam as JadeParamObject).Value as MethodNDict;
            }

        }

        [JadeSoftware.Joob.Client.JoobMethodAttribute("scriptSetSource")]
        public void ScriptSetSource(Script script, string source)
        {
            using (JadeParam jadeParam1 = new JadeParamObject(script, Usage.Input),
                             jadeParam2 = new JadeParamString(source))
            {
                this.SendMessage(_metaModel.scriptSetSource, null, jadeParam1, jadeParam2);
            }

        }

        [JadeSoftware.Joob.Client.JoobMethodAttribute("deleteMethod")]
        public void DeleteMethod(Routine method)
        {
            using (JadeParam jadeParam1 = new JadeParamObject(method, Usage.Input))
            {
                this.SendMessage(_metaModel.deleteMethod, null, jadeParam1);
            }

        }

        [JadeSoftware.Joob.Client.JoobMethodAttribute("addMethod")]
        public void AddMethod(Class type, string methodName)
        {
            using (JadeParam retnParam = new JadeParamObjectId(Usage.Output),
                             jadeParam1 = new JadeParamObject(type),
                             jadeParam2 = new JadeParamString(methodName))
            {
                this.SendMessage(_metaModel.addMethod, retnParam, jadeParam1, jadeParam2);
                //return JoobContext.CurrentContext.FindInstance<Routine>((retnParam as JadeParamObjectId).Value);
            }

        }



        #endregion
    }

    public partial class JadeMetaControllerAppMetadata : JadeSoftware.Joob.IDomainMetadata
	{

		internal JadeSoftware.Joob.ClassMetadata metaClass;
        internal JadeSoftware.Joob.MethodMetadata schemaGetClasses;
        internal JadeSoftware.Joob.MethodMetadata typeGetMethods;
        internal JadeSoftware.Joob.MethodMetadata scriptSetSource;
        internal JadeSoftware.Joob.MethodMetadata deleteMethod;
        internal JadeSoftware.Joob.MethodMetadata addMethod;

        private JadeMetaControllerAppMetadata()
		{
		}
		partial void InitializeDynamicProperties(JadeSoftware.Joob.Client.IJoobConnection connection);

		public JadeMetaControllerAppMetadata(JadeSoftware.Joob.Client.JoobConnection connection)
		{
			metaClass = new JadeSoftware.Joob.ClassMetadata(connection, typeof(JadeMetaControllerApp), "JadeMetaControllerApp", "RootSchema.JadeMetaControllerSchema");
			this.InitializeProperties(connection);
			this.InitializeDynamicProperties(connection);
		}

		private void InitializeProperties(JadeSoftware.Joob.Client.IJoobConnection connection)
		{
            schemaGetClasses = metaClass.CheckMethod(metaClass, "schemaGetClasses");
            typeGetMethods = metaClass.CheckMethod(metaClass, "typeGetMethods");
            scriptSetSource = metaClass.CheckMethod(metaClass, "scriptSetSource");
            deleteMethod = metaClass.CheckMethod(metaClass, "deleteMethod");
            addMethod = metaClass.CheckMethod(metaClass, "addMethod");
        }
    }
}
