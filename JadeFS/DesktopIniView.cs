﻿/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DokanNet;
using JadeSoftware.Joob.Client;

namespace JadeFileSystem
{
    class DesktopIniView : View
    {
        public override string Pattern => @"\\DESKTOP.INI$";

        public override void Cleanup(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override void CloseFile(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus CreateFile(JoobContext ctx, string fileName, DokanNet.FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info, Match match)
        {
            return NtStatus.ObjectNameInvalid;
        }

        public override NtStatus DeleteDirectory(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus DeleteFile(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus FindFiles(JoobContext ctx, string fileName, out IList<FileInformation> files, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus FindStreams(JoobContext ctx, string fileName, out IList<FileInformation> streams, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus FlushFileBuffers(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus GetFileInformation(JoobContext ctx, string fileName, out FileInformation fileInfo, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus GetFileSecurity(JoobContext ctx, string fileName, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus LockFile(JoobContext ctx, string fileName, long offset, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus MoveFile(JoobContext ctx, string oldName, string newName, bool replace, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus ReadFile(JoobContext ctx, string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetAllocationSize(JoobContext ctx, string fileName, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetEndOfFile(JoobContext ctx, string fileName, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetFileAttributes(JoobContext ctx, string fileName, FileAttributes attributes, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetFileSecurity(JoobContext ctx, string fileName, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetFileTime(JoobContext ctx, string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus UnlockFile(JoobContext ctx, string fileName, long offset, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus WriteFile(JoobContext ctx, string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }
    }
}

