﻿/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DokanNet;

using JadeSoftware.Joob;
using JadeSoftware.Joob.Client;
using JadeSoftware.Joob.Metadata;
using JadeSoftware.Joob.Management;
using JadeSoftware.Joob.MetaSchema;
using JadeSoftware.Jade.DotNetInterop;

namespace JadeFileSystem.Tool
{
    class Program
    {
        static void Main(string[] args)
        {
             
            JadeFS vfs = new JadeFS();
            Console.WriteLine("Mounting");
            Dokan.Mount(vfs, JadeFS.GetMountPoint(), DokanOptions.FixedDrive, 4);

        }
    }
}
