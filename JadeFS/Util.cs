﻿/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

using JadeSoftware.Joob.MetaSchema;
using JadeMetaExposure;
using JadeSoftware.Joob.Client;
using JadeSoftware.Jade.DotNetInterop;
using DokanNet;

namespace JadeFileSystem
{
    public static class Util
    {
        private static ConcurrentDictionary<string, Schema> schemasByName = new ConcurrentDictionary<string, Schema>();
        private static ConcurrentDictionary<string, Class> classesByName = new ConcurrentDictionary<string, Class>();

        public static string entityNamePattern = @"[A-Z](?i:[A-Z0-9_]){0,99}";

        public static Schema GetSchema(JoobContext ctx, string schemaName)
        {
            return schemasByName.GetOrAdd(schemaName.ToUpper(), schmemaNameUpper =>
            {
                List<Schema> schemas = new List<Schema>();

                Schema rootSchema = GetRootSchema(ctx);

                foreach (Schema schema in rootSchema.WithAllSubschemas())
                    if (schema.Name.ToUpper() == schmemaNameUpper)
                        schemas.Add(schema);

                if (schemas.Count == 1)
                    return schemas[0];
                else
                    return null;

            });
        }

        public static Schema GetSchema(JoobContext ctx, Match match)
        {
            Group schemaName = match.Groups["schema"];
            return GetSchema(ctx, schemaName.Value);
        }

        public static Class GetClass(JoobContext ctx, Schema schema, string className)
        {
            string classNameUpper = className.ToUpper();
            return classesByName.GetOrAdd(schema.Name.ToUpper() + "::" + classNameUpper, fqClassName =>
            {
                List<Class> classesMatched = new List<Class>();

                ClassNDict classes = GetApp(ctx).SchemaGetClasses(schema);

                foreach (Class cls in classes)
                    if (cls.Name.ToUpper() == classNameUpper)
                        classesMatched.Add(cls);

                if (classesMatched.Count == 1)
                    return classesMatched[0];
                else
                    return null;

            });
        }

        public static Class GetClass(JoobContext ctx, Match match)
        {
            Schema schema = GetSchema(ctx, match);
            if (schema == null)
                return null;
            Group classNames = match.Groups["type"];
            Capture className = classNames.Captures[classNames.Captures.Count - 1];
            return GetClass(ctx, schema, className.Value);
        }

        public static ClassNDict ClassGetDirectSubClasses(JoobContext ctx, Class cls)
        {
            ClassNDict subclasses = new ClassNDict(JadeSoftware.Joob.ClassPersistence.Transient);

            Class classy = cls;
            do
            {
                //Console.WriteLine("getting subclasses of " + classy.Name);

                foreach(Class subclass in classy.Subclasses)
                    if (!subclasses.ContainsKey(new ClassNDictKey(subclass.Name)))
                        subclasses.Add(subclass);
                
                JomType superType = classy.SuperschemaType;

                if (superType != null)
                    classy = superType as Class;
                else
                    classy = null;

            } while (classy != null);

            return subclasses;
        }

        public static Routine GetRoutine(JoobContext ctx, Class cls, string methodName)
        {
            List<Routine> methodsMatched = new List<Routine>();

            JadeMetaControllerApp app = GetApp(ctx);

            MethodNDict methods = app.TypeGetMethods(cls);

            foreach (Routine method in methods)
                if (method.Name.ToUpper() == methodName.ToUpper())
                    methodsMatched.Add(method);

            if (methodsMatched.Count == 1)
                return methodsMatched[0];
            else
                return null;

        }

        public static Routine GetRoutine(JoobContext ctx, Match match)
        {
            Class cls = GetClass(ctx, match);
            if (cls == null)
                return null;
            Group methodName = match.Groups["method"];
            return GetRoutine(ctx, cls, methodName.Value);
        }

        public static void AddDotDirs(List<FileInformation> files)
        {
            files.Add(NewDirectoryInfo("."));
            files.Add(NewDirectoryInfo(".."));
        }

        public static DateTime DefaultDate()
        {
            return new DateTime(2000, 1, 1);
        }

        public static FileInformation NewDirectoryInfo(string fileName)
        {
            return NewDirectoryInfo(fileName, false);
        }

        public static FileInformation NewDirectoryInfo(string fileName, bool hidden)
        {
            FileInformation fileInfo = new FileInformation();
            fileInfo.FileName = fileName;

            fileInfo.Attributes = FileAttributes.Directory;

            if (hidden)
                fileInfo.Attributes = fileInfo.Attributes | FileAttributes.Hidden;

            fileInfo.LastAccessTime = DefaultDate();
            fileInfo.LastWriteTime = DefaultDate();
            fileInfo.CreationTime = DefaultDate();

            return fileInfo;
        }


        public static Schema GetRootSchema(JoobContext ctx)
        {
            return ctx.FindInstance<Schema>(ctx.GetSystemVariables().Rootschema);
        }

        public static JadeMetaControllerApp GetApp(JoobContext ctx)
        {
            return ctx.FindInstance<JadeMetaControllerApp>(ctx.GetSystemVariables().App);
        }

        public static string ScriptGetSource(Script script)
        {
            string source;
            script.TryGetPropertyValue<string>("source", out source);
            return source;
        }

        public static FileInformation RoutineToFileInfo(Routine routine)
        {
            DateTime modifiedTimeStamp;

            routine.TryGetPropertyValue<DateTime>("modifiedTimeStamp", out modifiedTimeStamp);

            string source = ScriptGetSource(routine);
            byte[] sourceUTF8 = Encoding.UTF8.GetBytes(source);

            FileInformation finfo = new FileInformation();
            finfo.FileName = routine.Name;
            finfo.Attributes = System.IO.FileAttributes.Normal;
            finfo.LastAccessTime = modifiedTimeStamp;
            finfo.LastWriteTime = modifiedTimeStamp;
            finfo.CreationTime = modifiedTimeStamp;
            finfo.Length = sourceUTF8.Length;

            return finfo;
        }





    }
}
