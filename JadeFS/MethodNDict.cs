/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

namespace JadeMetaExposure
{
	using System;
	using System.Collections.Generic;
	using System.Runtime.Serialization;
	using JadeSoftware.Joob;
	using JadeSoftware.Joob.Client;
	using JadeSoftware.Joob.Metadata;
	using JadeSoftware.Joob.Management;
	using JadeSoftware.Joob.MetaSchema;
	using JadeSoftware.Jade.DotNetInterop;


	[System.Runtime.Serialization.DataContractAttribute(IsReference=true)]
	[JadeSoftware.Joob.Client.JoobDictionaryKeyClassAttribute(typeof(MethodNDict))]
	public partial class MethodNDictKey : JadeSoftware.Joob.JoobDictionaryKey
	{

		private static JadeSoftware.Joob.KeyMetadata _name = MetadataCache<MethodNDictMetadata>.GetData(null).MethodNDictKey;

		public MethodNDictKey() :
			base(new JadeSoftware.Joob.KeyMetadata[] {
					_name})
		{
		}

		public MethodNDictKey(String name) :
				this()
		{
			this.Name = name;
		}

		[JadeSoftware.Joob.Client.JoobDictionaryKeyAttribute("name", 0, typeof(String), Length = 100, CaseSensitive = true, Ascending = true, SortOrder = 0)]
		public String Name
		{
			get
			{
				return ((String)(this.Keys[0]));
			}
			set
			{
				this.Keys[0] = value;
			}
		}
	}

	[System.Runtime.Serialization.KnownTypeAttribute(typeof(Routine))]
	[System.Runtime.Serialization.KnownTypeAttribute(typeof(MethodNDictKey))]
	[System.Runtime.Serialization.CollectionDataContractAttribute(IsReference=true)]
	[System.ComponentModel.DataAnnotations.MetadataTypeAttribute(typeof(MethodNDictMetadata))]
	[JadeSoftware.Joob.Client.JoobClassAttribute("MethodNDict", "RootSchema", ClassNamespace = "JadeMetaExposure", DuplicatesAllowed = false)]
	[JadeSoftware.Joob.Metadata.JomlTypeAttribute(JadeSoftware.Joob.Metadata.JomlTypeKind.ClassCollection, "MethodNDict", typeof(MemberKeyDictionary<MethodNDictKey, Routine>), MembershipType=typeof(Routine), Keys="name")]
	public partial class MethodNDict : MemberKeyDictionary<MethodNDictKey, Routine>
	{

		private static MethodNDictMetadata _metaModel;

		partial void Initialize();


		static MethodNDict()
		{
			_metaModel = MetadataCache<MethodNDictMetadata>.GetData(null);
		}

		private MethodNDict() : 
				this(JadeSoftware.Joob.ClassPersistence.Transient)
		{
		}

		public MethodNDict(JadeSoftware.Joob.ClassPersistence lifetime) : 
				base(lifetime, typeof(MethodNDict), _metaModel.metaClass)
		{
			this.Initialize();
		}

		protected MethodNDict(JadeSoftware.Joob.ClassPersistence lifetime, System.Type type, JadeSoftware.Joob.ClassMetadata metaClass) :
		        base(lifetime, type, metaClass)
		{
		    this.Initialize();
		}

		public virtual Routine this[String name]
		{
			get
			{
				MethodNDictKey key = new MethodNDictKey(name);
				 return base[key];
			}
			set
			{
				MethodNDictKey key = new MethodNDictKey(name);
				base[key] = value;
			}
		}

		public virtual bool TryGetValue(String name, out Routine value)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			return base.TryGetValue(key, out value);
		}

		public virtual bool TryGetValue(String name, JadeSoftware.Joob.SearchStrategy strategy, out Routine value)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			return base.TryGetValue(key, strategy, out value);
		}

		public virtual IJoobDictionaryEnumerable<MethodNDictKey, Routine> StartingAtKey(String name)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			return base.StartingAtKey(key);
		}

		public virtual IJoobDictionaryEnumerable<MethodNDictKey, Routine> StartingAtKey(String name, JadeSoftware.Joob.SearchStrategy strategy)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			return base.StartingAtKey(key, strategy);
		}

		public virtual void Remove(String name)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			base.Remove(key);
		}

		public virtual void Remove(String name, Routine member)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			base.Remove(key, member);
		}

		public virtual bool ContainsKey(String name)
		{
			MethodNDictKey key = new MethodNDictKey(name);
			return base.ContainsKey(key);
		}
	}

	[System.Runtime.Serialization.KnownTypeAttribute(typeof(Routine))]
	[System.Runtime.Serialization.KnownTypeAttribute(typeof(MethodNDictKey))]
	[System.Runtime.Serialization.CollectionDataContractAttribute(IsReference=true)]
	[System.ComponentModel.DataAnnotations.MetadataTypeAttribute(typeof(MethodNDictMetadata))]
	[JadeSoftware.Joob.Client.JoobClassAttribute("MethodNDict", "RootSchema.Test", ClassNamespace= "JadeMetaExposure")]
	public abstract partial class MethodNDict<TKey, TValue> : MemberKeyDictionary<TKey, TValue>
		where TKey : JadeSoftware.Joob.IJoobDictionaryKey, new ()
		where TValue : Routine
    {

		private static MethodNDictMetadata _metaModel;

		partial void Initialize();


		static MethodNDict()
		{
			_metaModel = MetadataCache<MethodNDictMetadata>.GetData(null);
		}

		protected MethodNDict(JadeSoftware.Joob.ClassPersistence lifetime, System.Type type, JadeSoftware.Joob.ClassMetadata metaClass) :
		        base(lifetime, type, metaClass)
		{
		    this.Initialize();
		}
	}

	public partial class MethodNDictMetadata : JadeSoftware.Joob.IDomainMetadata
	{

		internal JadeSoftware.Joob.ClassMetadata metaClass;

		internal JadeSoftware.Joob.KeyMetadata MethodNDictKey;

		private MethodNDictMetadata()
		{
		}

		public MethodNDictMetadata(JadeSoftware.Joob.Client.JoobConnection connection)
		{
			this.InitializeProperties(connection);
		}

		private void InitializeProperties(JadeSoftware.Joob.Client.JoobConnection connection)
		{
			metaClass = new JadeSoftware.Joob.ClassMetadata(connection, typeof(MethodNDict), "MethodNDict", "RootSchema", JadeSoftware.Joob.DuplicationOption.NotAllowed);
			this.MethodNDictKey = metaClass.CheckMemberKey(typeof(MethodNDict), typeof(Routine), "name", typeof(String), 100, JadeSoftware.Joob.CaseSensitivityOption.Sensitive, JadeSoftware.Joob.KeyOrderOption.Ascending);
		}
	}
}
