﻿/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using System.Text.RegularExpressions;
using System.Threading;

using DokanNet;

using JadeSoftware.Joob.Client;
using JadeSoftware.Joob.MetaSchema;

namespace JadeFileSystem
{
    public class JadeFS : DokanNet.IDokanOperations
    {

        private ThreadLocal<JoobContext> context = new ThreadLocal<JoobContext>(() =>
        {
            JoobConnection conn = new JoobConnection();
            conn.Open();
            JoobContext ctx = new JoobContext(conn);
            return ctx;
        }, true);

        private ConcurrentDictionary<string, Schema> schemasByName = new ConcurrentDictionary<string, Schema>();
        private ConcurrentDictionary<string, Class> classesByName = new ConcurrentDictionary<string, Class>();

        private static IEnumerable<View> YieldAllViews()
        {
            yield return new AllSchemasView();
            yield return new SchemaView();
            yield return new TypeView();
            yield return new ClassMethodsView();
            yield return new MethodView();
            yield return new DesktopIniView();
        }

        private static IEnumerable<KeyValuePair<string, View>> YieldAllViewsByPattern()
        {
            foreach(View view in YieldAllViews())
                yield return new KeyValuePair<string, View>(view.Pattern, view);
        }

        private ConcurrentBag<View> views = new ConcurrentBag<View>(YieldAllViews());

        public static string GetMountPoint()
        {
            return "C:\\JadeFS\\mount-point\\";
        }

        public void Cleanup(string fileName, DokanFileInfo info)
        {
        }

        public void CloseFile(string fileName, DokanFileInfo info)
        {
            try
            {
                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                    {
                        view.CloseFile(ctx, fileName, info, match);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.ToString());
            }


        }

        public NtStatus CreateFile(string fileName, DokanNet.FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info)
        {
            try
            {
                //Console.WriteLine("CreateFile called on " + fileName + " mode = " + mode.ToString() + " options = " + options.ToString());

                JoobContext ctx = this.context.Value;

                foreach(View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                    {
                        //Console.WriteLine("CreateFile called on " + view.ToString() + " for file " + fileName);
                        return view.CreateFile(ctx, fileName, access, share, mode, options, attributes, info, match);
                    }
                }

                Console.WriteLine("unhandled pattern within CreateFile. fileName = " + fileName);

                return NtStatus.ObjectNameNotFound;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.ToString());
                return NtStatus.Error;
            }
        }

        public NtStatus DeleteDirectory(string fileName, DokanFileInfo info)
        {
            Console.WriteLine("DeleteDirectory called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus DeleteFile(string fileName, DokanFileInfo info)
        {
            try
            {
                Console.WriteLine("DeleteFile called on " + fileName);

                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                        return view.DeleteFile(ctx, fileName, info, match);
                }

                return NtStatus.Error;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.ToString());
                return NtStatus.Error;
            }
        }

        public NtStatus FindFiles(string fileName, out IList<FileInformation> files, DokanFileInfo info)
        {
            try
            {
                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                        return view.FindFiles(ctx, fileName, out files, info, match);
                }

                Console.WriteLine("unhandled pattern within FindFiles. fileName = " + fileName);

                files = null;
                return NtStatus.ObjectNameNotFound;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.ToString());
                files = null;
                return NtStatus.Error;
            }

        }

        public NtStatus FindStreams(string fileName, out IList<FileInformation> streams, DokanFileInfo info)
        {
            //Console.WriteLine("FindStreams " + fileName);
            streams = null;
            return NtStatus.NotImplemented;
        }

        public NtStatus FlushFileBuffers(string fileName, DokanFileInfo info)
        {
            Console.WriteLine("FlushFileBuffers " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus GetDiskFreeSpace(out long freeBytesAvailable, out long totalNumberOfBytes, out long totalNumberOfFreeBytes, DokanFileInfo info)
        {
            Console.WriteLine("GetDiskFreeSpace ");
            freeBytesAvailable = 100;
            totalNumberOfBytes = 200;
            totalNumberOfFreeBytes = 100;
            return NtStatus.Success;
        }

        public NtStatus GetFileInformation(string fileName, out FileInformation fileInfo, DokanFileInfo info)
        {
            //Console.WriteLine("GetFileInformation " + fileName);
            try
            {
                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                        return view.GetFileInformation(ctx, fileName, out fileInfo, info, match);
                }

                Console.WriteLine("unhandled pattern within GetFileInformation. fileName = " + fileName);
                fileInfo = new FileInformation();
                fileInfo.FileName = fileName;
                return NtStatus.ObjectNameNotFound;

            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFileInformation error on file " + fileName + " " + ex.ToString());
                fileInfo = new FileInformation();
                fileInfo.FileName = fileName;
                return NtStatus.Error;
            }
        }

        public NtStatus GetFileSecurity(string fileName, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            try
            {
                //Console.WriteLine("GetFileSecurity called on " + fileName);

                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                        return view.GetFileSecurity(ctx, fileName, out security, sections, info, match);
                }

                Console.WriteLine("unhandled pattern within GetFileSecurity. fileName = " + fileName);
                security = null;
                return NtStatus.ObjectNameNotFound;

            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFileSecurity error on file " + fileName + " " + ex.ToString());
                security = null;
                return NtStatus.Error;
            }
        }

        public NtStatus GetVolumeInformation(out string volumeLabel, out FileSystemFeatures features, out string fileSystemName, DokanFileInfo info)
        {
            Console.WriteLine("GetVolumeInformation");
            volumeLabel = "JadeFS";
            features = FileSystemFeatures.CasePreservedNames | FileSystemFeatures.CaseSensitiveSearch;
            fileSystemName = "JadeFS";
            return NtStatus.Success;
        }

        public NtStatus LockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            Console.WriteLine("LockFile called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus Mounted(DokanFileInfo info)
        {
            return NtStatus.NotImplemented;
        }

        public NtStatus MoveFile(string oldName, string newName, bool replace, DokanFileInfo info)
        {
            Console.WriteLine("MoveFile called on " + oldName);
            return NtStatus.NotImplemented;
        }

        public NtStatus ReadFile(string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info)
        {
            try
            {
                Console.WriteLine("ReadFile called on " + fileName + " offset = " + offset.ToString());

                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                        return view.ReadFile(ctx, fileName, buffer, out bytesRead, offset, info, match);
                }

                Console.WriteLine("unhandled pattern within ReadFile. fileName = " + fileName);
                bytesRead = 0;
                return NtStatus.ObjectNameNotFound;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ReadFile error on file " + fileName + " " + ex.ToString());
                bytesRead = 0;
                return NtStatus.Error;
            }
        }

        public NtStatus SetAllocationSize(string fileName, long length, DokanFileInfo info)
        {
            Console.WriteLine("SetAllocationSize called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus SetEndOfFile(string fileName, long length, DokanFileInfo info)
        {
            Console.WriteLine("SetEndOfFile called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus SetFileAttributes(string fileName, FileAttributes attributes, DokanFileInfo info)
        {
            Console.WriteLine("SetFileAttributes called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus SetFileSecurity(string fileName, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            Console.WriteLine("SetFileSecurity called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus SetFileTime(string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info)
        {
            Console.WriteLine("SetFileTime called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus UnlockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            Console.WriteLine("UnlockFile called on " + fileName);
            return NtStatus.NotImplemented;
        }

        public NtStatus Unmounted(DokanFileInfo info)
        {
            
            foreach(JoobContext ctx in this.context.Values)
            {
                JoobConnection conn = ctx.Connection;
                ctx.Dispose();
                conn.Dispose();
            }
            return NtStatus.Success;
        }


        public NtStatus WriteFile(string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info)
        {
            try
            {
                Console.WriteLine("WriteFile called on " + fileName);

                JoobContext ctx = this.context.Value;

                foreach (View view in this.views)
                {
                    Match match = Regex.Match(fileName, view.Pattern);
                    if (match.Success)
                        return view.WriteFile(ctx, fileName, buffer, out bytesWritten, offset, info, match);
                }

                bytesWritten = 0;
                return NtStatus.ObjectNameInvalid;
            }
            catch (Exception ex)
            {
                Console.WriteLine("error within WriteFile on fileName = " + fileName + " " + ex.ToString());
                bytesWritten = 0;
                return NtStatus.Error;
            }
        }
    }
}
