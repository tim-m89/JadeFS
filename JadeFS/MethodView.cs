﻿/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DokanNet;
using JadeSoftware.Joob.MetaSchema;
using JadeSoftware.Joob.Client;

namespace JadeFileSystem
{
    public struct OpenedFile
    {
        public DokanNet.FileAccess access;
        public FileShare share;
        public FileMode mode;
        public FileOptions options;
    }

    class MethodView : View
    {
        public override string Pattern => @"^(?i)\\(?<schema>" + Util.entityNamePattern + @")(?:\\(?<type>" + Util.entityNamePattern + @"))+\\methods\\(?<method>[A-Z_]+[A-Z0-9_]*)$";

        private ConcurrentDictionary<Tuple<string, int>, OpenedFile> openedFiles = new ConcurrentDictionary<Tuple<string, int>, OpenedFile>();

        public override void Cleanup(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
        }

        public override void CloseFile(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            Tuple<string, int> fileNameAndProcess = Tuple.Create<string, int>(fileName.ToUpper(), info.ProcessId);

            OpenedFile file;

            if (openedFiles.TryRemove(fileNameAndProcess, out file) )
            {
                if (file.options == FileOptions.DeleteOnClose)
                    DeleteFile(ctx, fileName, info, match);
            }
            else
            {
                Console.WriteLine("Close file called on unknown file " + fileName);
            }
           
        }

        public override NtStatus CreateFile(JoobContext ctx, string fileName, DokanNet.FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info, Match match)
        {
            Console.WriteLine("CreateFile called on " + fileName + " mode = " + mode.ToString() + " options = " + options.ToString());

            if (mode == FileMode.Append || mode == FileMode.Truncate)
            {
                return NtStatus.Error;  // TODO
            }

            Routine method = Util.GetRoutine(ctx, match);

            if (method == null && mode == FileMode.Open)
                return NtStatus.ObjectNameNotFound;

            if (method != null && mode == FileMode.CreateNew)
                return NtStatus.ObjectNameCollision;

            if(method == null && mode == FileMode.Create || mode == FileMode.CreateNew || mode == FileMode.OpenOrCreate)
            {
                Class cls = Util.GetClass(ctx, match);
                Group methodName = match.Groups["method"];
                Util.GetApp(ctx).AddMethod(cls, methodName.Value);
            }

            Tuple<string,int> fileNameAndProcess = Tuple.Create<string, int>(fileName.ToUpper(), info.ProcessId);

            openedFiles.AddOrUpdate(fileNameAndProcess,
                key => {
                    OpenedFile file = new OpenedFile();
                    file.access = access;
                    file.share = share;
                    file.mode = mode;
                    file.options = options;
                    return file;
                },
                (key, file) => {
                    file.access = access;
                    file.share = share;
                    file.mode = mode;
                    file.options = options;
                    return file;
                }
            );

            return NtStatus.Success;
        }

        public override NtStatus DeleteDirectory(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus DeleteFile(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            Routine method = Util.GetRoutine(ctx, match);

            if (method == null)
            {
                Console.WriteLine("Could not find method for file " + fileName);
                return NtStatus.ObjectNameNotFound;
            }

            Util.GetApp(ctx).DeleteMethod(method);
            return NtStatus.Success;
        }

        public override NtStatus FindFiles(JoobContext ctx, string fileName, out IList<FileInformation> files, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus FindStreams(JoobContext ctx, string fileName, out IList<FileInformation> streams, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus FlushFileBuffers(JoobContext ctx, string fileName, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus GetFileInformation(JoobContext ctx, string fileName, out FileInformation fileInfo, DokanFileInfo info, Match match)
        {
            Routine method = Util.GetRoutine(ctx, match);

            if (method == null)
            {
                Console.WriteLine("Could not find method for file " + fileName);
                fileInfo = new FileInformation();
                return NtStatus.ObjectNameNotFound;
            }

            fileInfo = Util.RoutineToFileInfo(method);
            return NtStatus.Success;
        }

        public override NtStatus GetFileSecurity(JoobContext ctx, string fileName, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info, Match match)
        {
            security = new FileSecurity();
            return NtStatus.Success;
        }

        public override NtStatus LockFile(JoobContext ctx, string fileName, long offset, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus MoveFile(JoobContext ctx, string oldName, string newName, bool replace, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus ReadFile(JoobContext ctx, string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info, Match match)
        {
            Routine method = Util.GetRoutine(ctx, match);

            if (method == null)
            {
                Console.WriteLine("Could not find method for file " + fileName);
                bytesRead = 0;
                return NtStatus.ObjectNameNotFound;
            }

            string source = Util.ScriptGetSource(method);
            byte[] sourceUTF8 = Encoding.UTF8.GetBytes(source);

            bytesRead = 0;
            for (long index = 0; index + 1 <= buffer.Length && index + offset + 1 <= sourceUTF8.Length; index++)
            {
                buffer[index] = sourceUTF8[index + offset];
                bytesRead = bytesRead + 1;
            }

            return NtStatus.Success;
        }

        public override NtStatus SetAllocationSize(JoobContext ctx, string fileName, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetEndOfFile(JoobContext ctx, string fileName, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetFileAttributes(JoobContext ctx, string fileName, FileAttributes attributes, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetFileSecurity(JoobContext ctx, string fileName, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus SetFileTime(JoobContext ctx, string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus UnlockFile(JoobContext ctx, string fileName, long offset, long length, DokanFileInfo info, Match match)
        {
            throw new NotImplementedException();
        }

        public override NtStatus WriteFile(JoobContext ctx, string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info, Match match)
        {
            if(offset != 0)
            {
                Console.WriteLine("Attempt to write at offset " + offset.ToString() + " of file " + fileName);
                bytesWritten = 0;
                return NtStatus.Error;
            }

            Routine method = Util.GetRoutine(ctx, match);

            if (method == null)
            {
                Console.WriteLine("Could not find method for file " + fileName);
                bytesWritten = 0;
                return NtStatus.ObjectNameNotFound;
            }

            string sourceNew = Encoding.UTF8.GetString(buffer);

            Util.GetApp(ctx).ScriptSetSource(method, sourceNew);

            bytesWritten = buffer.Length;

            return NtStatus.Success;
        }
    }
}

