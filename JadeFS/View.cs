﻿/* -----------------------------------------------------------------------------
 * Copyright (c). See file: LICENSE
 * ---------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DokanNet;
using JadeSoftware.Joob.Client;

namespace JadeFileSystem
{
    public abstract class View
    {
        public abstract string Pattern { get; }

        public abstract void Cleanup(JoobContext ctx, string fileName, DokanFileInfo info, Match match);
        public abstract void CloseFile(JoobContext ctx, string fileName, DokanFileInfo info, Match match);
        public abstract NtStatus CreateFile(JoobContext ctx, string fileName, DokanNet.FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info, Match match);
        public abstract NtStatus DeleteDirectory(JoobContext ctx, string fileName, DokanFileInfo info, Match match);
        public abstract NtStatus DeleteFile(JoobContext ctx, string fileName, DokanFileInfo info, Match match);
        public abstract NtStatus FindFiles(JoobContext ctx, string fileName, out IList<FileInformation> files, DokanFileInfo info, Match match);
        public abstract NtStatus FindStreams(JoobContext ctx, string fileName, out IList<FileInformation> streams, DokanFileInfo info, Match match);
        public abstract NtStatus FlushFileBuffers(JoobContext ctx, string fileName, DokanFileInfo info, Match match);
        public abstract NtStatus GetFileInformation(JoobContext ctx, string fileName, out FileInformation fileInfo, DokanFileInfo info, Match match);
        public abstract NtStatus GetFileSecurity(JoobContext ctx, string fileName, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info, Match match);
        public abstract NtStatus LockFile(JoobContext ctx, string fileName, long offset, long length, DokanFileInfo info, Match match);
        public abstract NtStatus MoveFile(JoobContext ctx, string oldName, string newName, bool replace, DokanFileInfo info, Match match);
        public abstract NtStatus ReadFile(JoobContext ctx, string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info, Match match);
        public abstract NtStatus SetAllocationSize(JoobContext ctx, string fileName, long length, DokanFileInfo info, Match match);
        public abstract NtStatus SetEndOfFile(JoobContext ctx, string fileName, long length, DokanFileInfo info, Match match);
        public abstract NtStatus SetFileAttributes(JoobContext ctx,string fileName, FileAttributes attributes, DokanFileInfo info, Match match);
        public abstract NtStatus SetFileSecurity(JoobContext ctx, string fileName, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info, Match match);
        public abstract NtStatus SetFileTime(JoobContext ctx, string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info, Match match);
        public abstract NtStatus UnlockFile(JoobContext ctx, string fileName, long offset, long length, DokanFileInfo info, Match match);
        public abstract NtStatus WriteFile(JoobContext ctx, string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info, Match match);
    }
}
