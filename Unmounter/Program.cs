﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JadeFileSystem;
using DokanNet;

namespace Unmounter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press enter to unmount");
            Console.ReadLine();
            Console.WriteLine("unmounting");
            Dokan.RemoveMountPoint(JadeFS.GetMountPoint());
        }
    }
}
