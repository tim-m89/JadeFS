/* JADE COMMAND FILE NAME C:\Users\Tim\Documents\Visual Studio 2015\Projects\JadeFS\jade-code\JadeMetaControllerSchema.jcf */
jadeVersionNumber "7.1.06";
schemaDefinition
JadeMetaControllerSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:12:12:10.662;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	1033 "English (United States)" schemaDefaultLocale;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:12:12:10.607;
libraryDefinitions
typeHeaders
	JadeMetaControllerApp subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2062;
	JadeMetaControllerGlobal subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2063;
	JadeMetaControllerSession subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2064;
 
interfaceDefs
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	JadeMetaControllerApp completeDefinition
	(
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:31:22:21:45.190;
 
	jadeMethodDefinitions
		addMethod(
			type: Type; 
			methodName: String): Method number = 1006;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:06:12:19:36:26.643;
		deleteMethod(meth: Method input) number = 1005;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:06:12:15:41:47.055;
		schemaGetClass(
			schema: Schema; 
			className: String): Class number = 1009;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:31:21:46:59.807;
		schemaGetClasses(schema: Schema): ClassNDict number = 1008;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:31:21:29:02.990;
		scriptSetSource(
			script: Script input; 
			source: String) number = 1004;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:31:23:25:15.115;
		sendCommandToServer(
			command: String; 
			args: ParamListType): Binary protected, number = 1001;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:14:51:07.155;
		sendExecuteToServer(source: String): Binary number = 1002;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:14:08:44.792;
		sendQuitToServer() number = 1003;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:14:09:01.552;
		typeGetMethod(
			type: Type; 
			methodName: String): Method number = 1011;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:31:21:53:57.524;
		typeGetMethods(type: Type): MethodNDict number = 1010;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:31:21:52:28.349;
	)
	FileNode completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.0.13" 11 2002:12:03:09:48:21.406;
	)
	File completeDefinition
	(
		setModifiedTimeStamp "Tim" "7.1.06" 2016:06:03:22:20:24.493;
 
	jadeMethodDefinitions
		fooBar() number = 1001;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:06:03:22:20:45.262;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	JadeMetaControllerGlobal completeDefinition
	(
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:12:12:10.656;
	)
	JadeScript completeDefinition
	(
 
	jadeMethodDefinitions
		closeServer() number = 1001;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:14:11:07.343;
		createClass() number = 1002;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:15:08:08.545;
		deleteClass() number = 1003;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:14:50:47.264;
	)
	Schema completeDefinition
	(
 
	jadeMethodDefinitions
		trmAddClass(
			className: String; 
			superclassName: String; 
			mapFileName: String; 
			classAccess: String; 
			classAbstract: Boolean; 
			transientByDefault: Boolean; 
			persistentAllowed: Boolean; 
			transientAllowed: Boolean; 
			sharedTransientAllowed: Boolean; 
			subclassPersistentAllowed: Boolean; 
			subclassTransientAllowed: Boolean; 
			subclassSharedTransientAllowed: Boolean; 
			classCollection: Boolean): Class number = 1001;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:15:04:59.860;
	)
	SchemaEntity completeDefinition
	(
	)
	Feature completeDefinition
	(
	)
	Script completeDefinition
	(
	)
	Routine completeDefinition
	(
	)
	Method completeDefinition
	(
 
	jadeMethodDefinitions
		trmDelete() number = 1001;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:06:12:15:53:31.887;
	)
	Type completeDefinition
	(
 
	jadeMethodDefinitions
		trmAddMethod(methodName: String): JadeMethod number = 1001;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:06:12:20:15:42.354;
	)
	Class completeDefinition
	(
 
	jadeMethodDefinitions
		trmDelete() number = 1001;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:14:50:47.284;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	JadeMetaControllerSession completeDefinition
	(
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:12:12:10.662;
	)
	Collection completeDefinition
	(
	)
	Btree completeDefinition
	(
	)
	Dictionary completeDefinition
	(
	)
	MemberKeyDictionary completeDefinition
	(
	)
	MethodNDict completeDefinition
	(
 
	jadeMethodDefinitions
		toWrapper() number = 1001;
		setModifiedTimeStamp "Tim" "7.1.06" 2016:05:29:12:10:16.327;
	)
 
inverseDefinitions
databaseDefinitions
JadeMetaControllerSchemaDb
	(
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:12:12:10.662;
	databaseFileDefinitions
		"JadeMetaController" number=52;
		setModifiedTimeStamp "Tim2" "7.1.06" 2016:05:28:12:12:10.662;
	defaultFileDefinition "JadeMetaController";
	classMapDefinitions
		JadeMetaControllerSession in "_environ";
		JadeMetaControllerApp in "_usergui";
		JadeMetaControllerGlobal in "JadeMetaController";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	JadeMetaControllerApp (
	jadeMethodSources
addMethod
{
addMethod(type : Type; methodName : String) : Method;

begin
	return type.trmAddMethod(methodName);
end;

}

deleteMethod
{
deleteMethod(meth : Method input);

begin
	meth.trmDelete();
end;

}

schemaGetClass
{
schemaGetClass(schema : Schema; className : String) : Class;

vars
	classes	: ClassNDict;
begin
	classes	:= schemaGetClasses( schema );
	return classes[ className ];
end;

}

schemaGetClasses
{
schemaGetClasses(schema : Schema) : ClassNDict;

begin
	return schema.getPropertyValue( Schema::classes.name ).ClassNDict;
end;

}

scriptSetSource
{
scriptSetSource(script : Script input; source : String);

begin
	beginTransaction;
	script.setPropertyValue( Script::source.name, source );
	if script.isKindOf( JadeMethod ) then
		script.JadeMethod._compile();
	endif;
	script.setPropertyValue( SchemaEntity::modifiedTimeStamp.name, app.actualTimeAppServer() );
	commitTransaction;
end;

}

sendCommandToServer
{
sendCommandToServer(command : String; args : ParamListType) : Binary protected;

constants
	PipeName	= "tmJadeMetaPipe";
	CmdLen		= 32;
vars
	pipe		: NamedPipe;
	length		,
	index		: Integer;
	param		: Any;
	result		: Binary;
begin
	exclusiveLock( global );
	
	create pipe transient;
	pipe.name	:= PipeName;
	pipe.open();
	
	pipe.writeBinary( command.padBlanks( CmdLen ).Binary );
	
	length	:= app.getParamListTypeLength( args );
	
	if length > 0 then
		
		foreach index in 1 to length do
			param	:= app.getParamListTypeEntry( index, args );
			pipe.writeBinary( param.Binary );
		endforeach;
		
	endif;
	
	if command <> "quit" then
		length	:= pipe.readBinary( 4 ).Integer;
	endif;
	
	if length > 0 then
		result	:= pipe.readBinary( length );
	endif;
	
	return result;
	
epilog
	if pipe <> null then
		pipe.close();
		delete pipe;
	endif;
	unlock( global );
end;

}

sendExecuteToServer
{
sendExecuteToServer(source : String) : Binary;

begin
	return sendCommandToServer( "execute", source.length(), source );
end;

}

sendQuitToServer
{
sendQuitToServer();

begin
	sendCommandToServer( "quit" );
end;

}

typeGetMethod
{
typeGetMethod(type : Type; methodName : String) : Method;

begin
	return typeGetMethods( type )[ methodName ];
end;

}

typeGetMethods
{
typeGetMethods(type : Type) : MethodNDict;

begin
	return type.getPropertyValue( Type::methods.name ).MethodNDict;
end;

}

	)
	File (
	jadeMethodSources
fooBar
{
fooBar();

vars

begin
	write method.name;
end;

}

	)
	JadeScript (
	jadeMethodSources
closeServer
{
closeServer();

begin
	app.sendQuitToServer();
end;

}

createClass
{
createClass();

begin
	rootSchema.getSchema("Test").trmAddClass( "CreateDelClass",
											  "Object",
											  null,
											  null,
											  false,
											  false,
											  false,
											  true,
											  false,
											  false,
											  false,
											  false,
											  false);
end;

}

deleteClass
{
deleteClass();

vars
	class	: Class;
begin
	class	:= rootSchema.getSchema("Test").getClass("CreateDelClass");
	class.trmDelete();	
end;

}

	)
	Schema (
	jadeMethodSources
trmAddClass
{
trmAddClass( className						,
			 superclassName					: String	;
			 mapFileName					: String	;
			 
			 classAccess					: String	;
			 classAbstract					: Boolean	;
			 
			 transientByDefault				,
			 
			 persistentAllowed				,
			 transientAllowed				,
			 sharedTransientAllowed			: Boolean	;
			 
			 subclassPersistentAllowed		,
			 subclassTransientAllowed		,
			 subclassSharedTransientAllowed	: Boolean	;
			 
			 classCollection				: Boolean	) : Class;

vars
	source	: String;
	class	: Class;
begin
	source	:= "
vars
	schema	: Schema;
	class	: Class;
begin
	schema	:= rootSchema.getSchema( '" & self.name & "' );
	beginTransaction;
		class	:= schema._addClass( '" & className & "', '" & superclassName & "', '" & mapFileName & "', '" & classAccess & "', " & classAbstract.String & ", " & transientByDefault.String & ", false, " & classCollection.String & ");
		class.setPropertyValue( 'persistentAllowed', " & persistentAllowed.String & " );
		class.setPropertyValue( 'transientAllowed', " & transientAllowed.String & " );
		class.setPropertyValue( 'sharedTransientAllowed', " & sharedTransientAllowed.String & " );
		class.setPropertyValue( 'subclassPersistentAllowed', " & subclassPersistentAllowed.String & " );
		class.setPropertyValue( 'subclassTransientAllowed', " & subclassTransientAllowed.String & " );
		class.setPropertyValue( 'subclassSharedTransientAllowed', " & subclassSharedTransientAllowed.String & " );
	commitTransaction;
	return class.getOidString().Binary;
end;";
	
	class	:= app.sendExecuteToServer( source ).String.asOid().Class;

	return class;
end;

}

	)
	Method (
	jadeMethodSources
trmDelete
{
trmDelete();

vars
	source	: String;
begin
	source	:= "
vars
	schema	: Schema;
	class	: Class;
	meth	: Method;
begin
	schema	:= rootSchema.getSchema( '" & self.schemaType.schema.name & "' );
	class	:= schema.getClass( '" & self.schemaType.name & "' );
	meth	:= class.getPropertyValue( Type::methods.name ).MethodNDict[ '" & self.name & "' ];
	beginTransaction;
	meth._delete();
	commitTransaction;
	return null;
end;
";
	app.sendExecuteToServer( source );
end;

}

	)
	Type (
	jadeMethodSources
trmAddMethod
{
trmAddMethod(methodName : String) : JadeMethod;

vars
	source	: String;
	meth	: JadeMethod;
begin
	source	:= "
vars
	schema	: Schema;
	cls		: Class;
	mth		: JadeMethod;
begin
	schema	:= rootSchema.getSchema( '" & self.schema.name & "' );
	cls		:= schema.getClass( '" & self.name & "' );
	beginTransaction;
	create mth persistent;
	mth.setPropertyValue( 'name', '" & methodName & "' );
	cls.addMethodLatest(mth, true);
	commitTransaction;
	return mth.getOidString().Binary;
end;";
	
	meth	:= app.sendExecuteToServer( source ).String.asOid().JadeMethod;

	return meth;
end;
}

	)
	Class (
	jadeMethodSources
trmDelete
{
trmDelete();

vars
	source	: String;
begin
	source	:= "
vars
	schema	: Schema;
	class	: Class;
begin
	schema	:= rootSchema.getSchema( '" & self.schema.name & "' );
	class	:= schema.getClass( '" & self.name & "' );
	beginTransaction;
	delete class;
	commitTransaction;
	return null;
end;
";
	app.sendExecuteToServer( source );
end;

}

	)
	MethodNDict (
	jadeMethodSources
toWrapper
{
toWrapper() : MethodWrapperByName;

vars
	coll	: MethodWrapperByName;
	wrapper	: MethodWrapper;
	meth	: Method;
begin
	create coll transient;
	
	foreach meth in self do
		wrapper	:= meth.toWrapper();
		coll.putAtKey( wrapper.mySchemaEntity.name, wrapper );
	endforeach;
	
	return coll;
end;

}

	)
